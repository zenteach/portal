import json
from collections import OrderedDict
from secrets import token_hex
from typing import List, Optional

import pytest
from factory import build
from flask.testing import FlaskClient
from pytest_mock import MockFixture
from server.models import Teacher
from server.tests.factories import InvitationFactory, TeacherFactory


@pytest.fixture
def teacher_(client: FlaskClient) -> Teacher:
    return TeacherFactory(with_school_class=True,  # nosec
                          email="joemama@example.com", password="Asdfgh12!")


@pytest.fixture
def unique_email() -> str:
    return token_hex(16) + "@example.com"


@pytest.fixture
def token() -> str:
    return token_hex(8)


@pytest.mark.parametrize(
    "email,password,message,status_code",
    [
        pytest.param(
            "", "", {'message': 'User  doesn\'t exist'}, 404
        ),
        pytest.param(
            "joemama@example.com", "123456789", {'message': 'Wrong credentials'}, 400
        ),
        pytest.param(
            "joe@example.com", "123456789", {'message': 'User joe@example.com doesn\'t exist'}, 404
        ),
        pytest.param(
            "joemama@example.com", "Asdfgh12!", {
                'message': 'Logged in as joemama@example.com'}, 200
        )
    ],
)
def test_teacher_login_endpoint(client: FlaskClient,
                                teacher_: Teacher,
                                email: str,
                                password: str,
                                mocker: MockFixture,
                                message: str,
                                access_token: List[str],
                                status_code: int):

    @mocker.patch('server.api.teacher_auth_api.log_activity')
    def patch_activity_log(*args):
        assert len(args) == 6
    response = client.post(
        '/api/v1/teacher/login',
        json={
            "email": email,
            "password": password
        }
    )
    assert response.status_code == status_code
    data = json.loads(response.data.decode())
    assert data == message


def test_teacher_registration_endpoint(client: FlaskClient,
                                       teacher_: Teacher,
                                       mocker: MockFixture):
    mocker.patch('server.api.teacher_auth_api.send_confirmation_instructions')
    new_teacher = build(dict, FACTORY_CLASS=TeacherFactory, with_invite_code=True)
    response = client.post(
        '/api/v1/teacher/registration',
        json=new_teacher
    )

    assert response.status_code == 400

    invite = InvitationFactory.create(invitation_from_id=teacher_.id)
    valid_teacher_payload = build(dict,
                                  FACTORY_CLASS=TeacherFactory,
                                  email=invite.email,
                                  invitation_code=invite.token)

    success_response = client.post(
        '/api/v1/teacher/registration',
        json=valid_teacher_payload
    )

    assert success_response.status_code == 200
    assert success_response.headers.get_all('Set-Cookie') is not None
    assert len(success_response.headers.get_all('Set-Cookie')) == 4


def test_teacher_registration_endpoint_with_exempted_invite_code(client: FlaskClient,
                                                                 token: str,
                                                                 unique_email: str,
                                                                 mocker: MockFixture):
    mocker.patch('server.api.teacher_auth_api.send_confirmation_instructions')

    def mock__load_exempt_list():
        return OrderedDict({'exempts': [token]})

    mocker.patch('server.config._load_exempt_list', autospec=mock__load_exempt_list,
                 side_effect=mock__load_exempt_list)

    new_teacher = build(dict, FACTORY_CLASS=TeacherFactory, email=unique_email)
    new_teacher.update({
        'invitation_code': token
    })
    response = client.post(
        '/api/v1/teacher/registration',
        json=new_teacher
    )

    assert response.status_code == 200
    assert response.headers.get_all('Set-Cookie') is not None
    assert len(response.headers.get_all('Set-Cookie')) == 4


def test_teacher_account_confirmation(client: FlaskClient, teacher_: Teacher):
    token = teacher_.generate_confirmation_token()

    response = client.post(
        '/api/v1/teacher/confirm',
        json={
            "token": token
        }
    )

    assert response.status_code == 200
    assert teacher_.confirmed


def test_teacher_account_recovery(client: FlaskClient,
                                  teacher_: Teacher,
                                  mocker: MockFixture):
    token = teacher_.get_reset_token()
    new_psswd = "FooPassword"

    @mocker.patch('server.api.teacher_auth_api.send_account_recovery_instructions.delay')
    def test_reset_get(send_account_recovery_instructions_delay):
        response = client.get(
            f'/api/v1/teacher/reset-password?email={teacher_.email}'
        )

        send_account_recovery_instructions_delay.assert_called_with(teacher_.email)
        assert response.status_code == 200

    response = client.post(
        '/api/v1/teacher/reset-password',
        json={
            "email": teacher_.email,
            "password": new_psswd,
            "token": token
        }
    )

    assert response.status_code == 200
    assert teacher_.password != new_psswd

    relogin_response = client.post(
        '/api/v1/teacher/login',
        json={
            "email": teacher_.email,
            "password": new_psswd
        }
    )

    assert relogin_response.status_code == 200


def test_teacher_user_invite(client: FlaskClient,
                             teacher_: Teacher,
                             teacher_identity: Teacher,
                             access_token: List[str],
                             mocker: MockFixture):

    invite_email = "invitee@someevent.com"

    @mocker.patch('server.api.teacher_auth_api.send_user_invite.delay')
    def test_invite_email(send_user_invite_delay):
        send_user_invite_delay.assert_called_with(invite_email)

    def patch_activity_log(*args):
        assert len(args) != 6
    mocker.patch('server.api.teacher_auth_api.log_activity', patch_activity_log)

    response = client.post(
        '/api/v1/teacher/invite',
        json={
            'email': invite_email
        },
        headers={
            'Authorization': 'Bearer {}'.format(access_token[0]),
            'X-CSRF-ACCESS-TOKEN': access_token[2]
        }
    )
    assert response.status_code == 200


def test_teacher_token_refresh(client: FlaskClient,
                               teacher_: Teacher,
                               teacher_identity: Teacher,
                               mocker: MockFixture):

    @mocker.patch('server.api.teacher_auth_api.redis_store')
    def test_success(redis_store):
        response = client.post('/api/v1/teacher/token/refresh')

        redis_store.assert_called_once()
        assert response.status_code == 200


def test_teacher_logout(client: FlaskClient, teacher_: Teacher, mocker: MockFixture) -> None:
    mocker.patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
    mocker.patch('server.api.teacher_auth_api.get_jwt')
    mocker.patch('server.api.teacher_auth_api.redis_store')

    response = client.delete('/api/v1/teacher/logout')

    assert response.status_code == 204


def test_teacher_send_confirmation(client: FlaskClient,
                                   teacher_: Teacher,
                                   mocker: MockFixture) -> None:
    @mocker.patch('server.api.teacher_auth_api.send_confirmation_instructions', return_value=True)
    def _mocked_send_confirmation_instructions(send_confirmation_instructions):
        send_confirmation_instructions.assert_called_with(teacher_.id)

    mocker.patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")

    response = client.post(
        '/api/v1/teacher/send_confirmation',
        json={
            'email': teacher_.email
        })

    assert response.status_code == 200


@pytest.mark.parametrize('email,status', [
    pytest.param(None, 400),
    pytest.param('cnajsk@canjskdajs.casjd', 404)
])
def test_teacher_send_confirmation_error(client: FlaskClient,
                                         email: Optional[str],
                                         status: int,
                                         mocker: MockFixture) -> None:

    mocker.patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")

    response = client.post(
        '/api/v1/teacher/send_confirmation',
        json={
            'email': email
        })

    assert response.status_code == status


def test_teacher_send_confirmation_exception(client: FlaskClient,
                                             teacher_: Teacher,
                                             access_token: List[str],
                                             mocker: MockFixture) -> None:
    # mocker.patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")

    def _mock_send_confirmation_instructions(*args):
        return Exception()

    mocker.patch('server.api.teacher_auth_api.send_confirmation_instructions',
                 _mock_send_confirmation_instructions)
    response = client.post(
        '/api/v1/teacher/send_confirmation',
        json={
            'email': teacher_.email
        },
        headers={
            'Authorization': 'Bearer {}'.format(access_token[0]),
            'X-CSRF-ACCESS-TOKEN': access_token[2]
        })
    assert response.status_code == 500
