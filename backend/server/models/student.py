import random
import string
from typing import List, Union

from server import bcrypt, db
from server.models import ClassQuiz, SchoolClass, StudentAttendedClass
from server.models.base_model import BaseModelMixin
from server.models.user_account.confirmable import Confirmable
from sqlalchemy import event
from sqlalchemy.dialects.postgresql import UUID


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))  # nosec


class Student(BaseModelMixin, db.Model, Confirmable):

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=True)
    firstname = db.Column(db.String(256), nullable=False)
    lastname = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(128), nullable=False)
    role = db.Column(db.String(128), nullable=False)

    classes = db.relationship(
        "SchoolClass", secondary="student_attended_class", viewonly=True
    )

    quiz_sessions = db.relationship(
        "ClassQuiz", secondary="quiz_session", viewonly=True
    )

    quiz_grades = db.relationship(
        "ClassQuiz", secondary="quiz_grade", viewonly=True)

    confirmed = db.Column(db.Boolean, nullable=True, default=False)
    confirmed_at = db.Column(db.DateTime, nullable=True)
    confirmation_token = db.Column(db.String, nullable=True)

    def __init__(
        self,
        firstname,
        lastname,
        email,
        classes=[],
        password=None,
        role="Student",
        confirmed=False
    ):
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.confirmed = confirmed
        if password is None:
            self.password = bcrypt.generate_password_hash(randomString()).decode(
                "utf-8"
            )
        else:
            self.password = bcrypt.generate_password_hash(
                password).decode("utf-8")
        self.role = role
        # Note to self: Setting class like this doesn't work for some reason
        # self.classes = classes

    def __repr__(self) -> str:
        return "<Student %s>" % (self.firstname + " " + self.lastname)

    @property
    def full_name(self) -> str:
        return "{} {}".format(self.firstname, self.lastname)

    @property
    def is_anonymous(self) -> bool:
        return '-anonymous@zenteach.co.uk' in self.email

    def add_to_class(self, classroom: SchoolClass) -> None:
        """
            Adds student to class.
            e.g student.add_to_class(class)
        """
        StudentAttendedClass(self, classroom).save()

    def add_from_class_list(self, classrooms: List[SchoolClass]) -> None:
        """
            Adds classes to a student
        """
        [self.add_to_class(_class) for _class in classrooms]

    def add_quiz_score(self, quiz: ClassQuiz, response: dict) -> None:
        from .quiz_grade import QuizGrade

        self.quiz_grade.append(
            QuizGrade(student_id=self.id, class_quiz_id=quiz.id, responses=response)
        )

    def remove_class(self, class_: SchoolClass) -> None:
        class_attended: StudentAttendedClass = StudentAttendedClass.query\
            .filter_by(student_id=self.id, school_class_id=class_.id).first()
        class_attended.delete()

    def update_email(self, email: str) -> None:
        if email == "":
            return

        self.email = email
        self.save()

    @classmethod
    def remove_model(cls, model: 'Student') -> None:
        classes: List[StudentAttendedClass] = StudentAttendedClass.query.filter_by(
            student_id=model.id).all()

        if len(classes) > 0:
            for _class in classes:
                _class.delete()

        model.delete()

    @classmethod
    def create_from_list(cls, lst) -> None:
        if len(lst) == 0:
            return
        errors = []
        instance = None
        added_students = []
        for it in lst:
            try:
                instance = cls(**it)
                instance.save()
            except Exception as e:
                errors.append(str(e))
            else:
                added_students.append(instance)
        return errors, added_students

    @classmethod
    def create_anonymous_student(cls, **kwargs) -> 'Student':
        from secrets import token_hex
        password_ = kwargs.get('password', 'Asdfgh12!')

        item = cls(
            firstname="Anonymous",
            lastname="Student",
            email=f"{kwargs.get('email_prefix', token_hex(10))}-anonymous@zenteach.co.uk",
            classes=kwargs.get('classes', []),
            password=password_,
            role="Student",
            confirmed=kwargs.get('confirmed', False)
        )
        item.save()
        return item


@event.listens_for(Student.classes, "append")
def student_class_append_listener(target: Student,
                                  value: Union[List[SchoolClass], SchoolClass],
                                  initiator):
    if isinstance(value, list):
        [target.add_to_class(item) for item in value]
    else:
        target.add_to_class(value)


@event.listens_for(Student.classes, "remove")
def student_class_remove_listener(target, value, initiator):
    db.engine.execute(
        "DELETE FROM student_attended_class WHERE school_class_id=%s AND student_id=%s",
        value.id,
        target.id,
    )
