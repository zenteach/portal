"""empty message

Revision ID: 87a7d9fb2fe9
Revises: df3bc107a79d
Create Date: 2019-09-28 19:40:47.567763
Add question and question_choices tables
"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "87a7d9fb2fe9"
down_revision = "df3bc107a79d"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "question_choices",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("choice", sa.String(length=256), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "questions",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("question", sa.String(length=256), nullable=False),
        sa.Column(
            "question_type",
            sa.Enum(
                "multiple_choice",
                "short_answer",
                "long_answer",
                name="questiontypeenum",
            ),
            nullable=False,
        ),
        sa.Column("subject", sa.String(length=256), nullable=False),
        sa.Column("choice_id", sa.Integer(), nullable=False),
        sa.Column("question_weights", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["choice_id"], ["question_choices.id"],),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("questions")
    op.drop_table("question_choices")
    # ### end Alembic commands ###
