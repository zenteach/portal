"""empty message

Revision ID: 9c85ceb994ae
Revises: 5545740a08ad
Create Date: 2020-03-06 15:29:39.088149

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9c85ceb994ae"
down_revision = "5545740a08ad"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "quiz_grade", sa.Column(
            "id", sa.Integer(), autoincrement=True, nullable=False)
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("quiz_grade", "id")
    # ### end Alembic commands ###
