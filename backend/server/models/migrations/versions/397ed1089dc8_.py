"""empty message

Revision ID: 397ed1089dc8
Revises: 092c8101da79
Create Date: 2020-01-10 21:28:04.939802

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "397ed1089dc8"
down_revision = "092c8101da79"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "class_quiz_scores",
        sa.Column("responses", postgresql.JSON(
            astext_type=sa.Text()), nullable=True),
    )

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("class_quiz_scores", "responses")
    # ### end Alembic commands ###
