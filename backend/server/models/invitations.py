from datetime import datetime

from server import db
from server.models import Setting, Teacher
from server.models.base_model import BaseModelMixin


class Invitations(BaseModelMixin, db.Model):

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(128), nullable=False)
    token = db.Column(db.String(16), nullable=False)
    invite_sent_at = db.Column(db.DateTime, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=False)
    invitation_from_id = db.Column(db.Integer, db.ForeignKey(
        "teacher.id"), nullable=False)
    invitee_id = db.Column(db.Integer, db.ForeignKey(
        "teacher.id", name="invitations_invitee_id_fkey"), nullable=True)
    invitee = db.relationship("Teacher", backref="invitation", foreign_keys=[invitee_id])

    def __init__(self,
                 email,
                 token,
                 invitation_from_id,
                 active=True,
                 invite_sent_at=datetime.now()):

        self.email = email
        self.token = token
        self.invitation_from_id = invitation_from_id
        self.active = True
        self.invite_sent_at = invite_sent_at

    @classmethod
    def use_token(cls, email, token):
        row = cls.query.filter_by(email=email, token=token, active=True).first()
        setting = Setting()
        EXEMPT_LIST = setting.get('exempts')
        if token in EXEMPT_LIST:
            return
        row.active = False

        row.save()

    @classmethod
    def associate_invitee(cls, teacher: Teacher, token: str) -> None:
        _invitation = cls.query.filter_by(token=token, active=False).first()
        if _invitation is None:
            return

        _invitation.invitee = teacher
        _invitation.save()

    @classmethod
    def validate_email_invitation_token(cls, email, token):
        row = cls.query.filter_by(email=email, token=token, active=True).all()
        setting = Setting()
        if token in setting.get('exempts'):
            return True

        if len(row) == 0:
            return False

        return True
