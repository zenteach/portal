export default {
  getCurrentUser: state => {
    return state.current_user
  },
  getLoadingState: state => {
    return state.loading
  }
}
