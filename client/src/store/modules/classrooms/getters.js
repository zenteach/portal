export default {
  CLASSLIST: state => {
    return state.class_list
  },
  GETCURRENTCLASS: state => {
    return state.current_class
  }
}
