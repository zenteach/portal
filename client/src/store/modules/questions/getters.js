export default {
  QUESTIONLIST: state => {
    return state.questions
  },

  CURRENTQUESTION: state => {
    return state.current_question
  }
}
