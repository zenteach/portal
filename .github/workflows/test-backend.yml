name: Backend test CI

on:
  pull_request:
    paths:
      - 'backend/**'

jobs:
  backend:
    runs-on: ['ubuntu-latest']
    container: python:3.10-bullseye
    env:
      AUTHLIB_INSECURE_TRANSPORT: true
      APP_SETTINGS: server.config.TestingConfig
      BROKER_TRANSPORT: amqp
      BROKER_URL: amqp://zenteach:zenteach_broker@rabbitmq:5672/jobs
      CELERY_ALWAYS_EAGER: True
      CLIENT_URL: http://localhost:8080
      DATABASE_TEST_URL: postgresql://zenteach:cipass@postgres:5432/zenteach
      DEFAULT_MAIL_SENDER: test_sender@example.com
      DEFAULT_QUEUE: jobs
      FLASK_ENV: testing
      FLASK_ADMIN_SWATCH: journal
      RUNTIME: development
      REDIS_URL: redis://redis:6379/1
      RESULT_BACKEND: redis://redis:6379/0
      TMP_DIR: /tmp
      TEMPLATE_DIR: server/templates
    defaults:
      run:
        working-directory: backend
    services:
      redis:
        image: redis:5.0.7-alpine
        ports:
          - 6379:6379
      postgres:
        image: postgres:12.7-alpine
        ports:
          - 5432:5432
        env:
          POSTGRES_DB: zenteach
          POSTGRES_USER: zenteach
          POSTGRES_PASSWORD: cipass
        options: >-
          --health-cmd pg_isready
          --health-interval 10s
          --health-timeout 5s
          --health-retries 5
      rabbitmq:
        image: rabbitmq:alpine
        ports:
          - 5672:5672
        env:
          RABBITMQ_DEFAULT_USER: zenteach
          RABBITMQ_ERLANG_COOKIE: va21ef9BSZz4WUVgcGcacmsIQ09qlh7VAKWOgfznaCg
          RABBITMQ_DEFAULT_PASS: zenteach_broker
          RABBITMQ_DEFAULT_VHOST: jobs
    steps:
      - name: Checkout code
        uses: actions/checkout@v2

      - name: Install packages - step 1
        run: |
          wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
          echo "deb http://apt.postgresql.org/pub/repos/apt/ bullseye-pgdg main" >> /etc/apt/sources.list.d/pgdg.list && \
          echo "deb http://deb.debian.org/debian bullseye-backports main" >> /etc/apt/sources.list.d/sources.list && \
          apt-get update
      - name: Install packages - step 2
        run: |
          apt-get install -y build-essential git sudo python3-pip python3-setuptools python3-wheel python3-cffi python3-dev
      - name: Install packages - step 3
        run: |
          apt-get install -y libcairo2 libpango-1.0-0 libpangocairo-1.0-0 && \
          apt-get install -y libgdk-pixbuf2.0-0 libffi-dev shared-mime-info libnss3-dev libx11-xcb1
      - name: Install packages - step 4
        run: |
          apt-get install -y gfortran wget unzip curl zlib1g zlib1g-dev \
          libfreetype6 libfreetype6-dev libpng-dev libopenblas-dev \
          netcat-openbsd libssl-dev python3-scipy libcurl4-openssl-dev python3-pycurl \
          shared-mime-info libjpeg-dev fonts-open-sans
      - name: Install packages - step 5
        run: |
          apt-get install -y libpq-dev && pip install pipenv

      - name: Install dependencies
        run: pipenv install --system

      - name: Run migrations
        run: python manage.py db upgrade

      - name: Run db configuration
        run: python manage.py db_conf

      - name: Run tests
        run: |
          pytest --timeout 600 --cov=server/ --driver Chrome -m "not e2e"
